package com.roosleex.epam.homework.datavalidation;

public class View {
    public static final String DATA_VALID = "Data is valid!";
    public static final String DATA_INVALID = "Data is not valid!";
    public static final String ENTER_FNAME = "Enter first name";
    public static final String ENTER_SNAME = "Enter second name";
    public static final String ENTER_NICK = "Enter nickname";
    public static final String ENTER_MOB_TEL = "Enter mobile phone in format (000)000-00-00";
    public static final String ENTER_EMAIL = "Enter email";
    public static final String EQUAL_SIGN = " = ";

    /**
     * Print the message to console with new line sign at the end of the message.
     * @param message
     */
    public void printMessage(String message) {
        System.out.println(message);
    }

    /**
     * Print the message to console without new line sign at the end of the message.
     * @param message
     */
    public void printMessageNoLine(String message) {
        System.out.print(message);
    }

    public String concatText(String... str) {
        StringBuilder sb = new StringBuilder();
        for(String s : str) {
            sb.append(s);
        }
        return new String(sb);
    }
}
