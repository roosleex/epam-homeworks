package com.roosleex.epam.homework.datavalidation;

import java.util.HashMap;
import java.util.Scanner;

public class Controller {
    public static final String NAME_REGEXP = "^[A-Za-z]+[-_]*[A-Za-z]{3,20}$";
    public static final String NICK_REGEXP = "^[A-Za-z]+[-_0-9]*[A-Za-z0-9]{5,29}$";
    public static final String EMAIL_REGEXP = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
    public static final String MOBTEL_REGEXP = "^\\(\\d{3}\\)\\d{3}-{1}\\d{2}-{1}\\d{2}$";
    private Model model;
    private View view;
    private Scanner sc = new Scanner(System.in);

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    /**
     * Process user actions.
     */
    public void processUser() {
        HashMap data = new HashMap();
        if ((inputData("fname", NAME_REGEXP, view.concatText(View.ENTER_FNAME, View.EQUAL_SIGN), data)) &&
                (inputData("sname", NAME_REGEXP, view.concatText(View.ENTER_SNAME, View.EQUAL_SIGN), data)) &&
                (inputData("nickname", NICK_REGEXP, view.concatText(View.ENTER_NICK, View.EQUAL_SIGN), data)) &&
                (inputData("email", EMAIL_REGEXP, view.concatText(View.ENTER_EMAIL, View.EQUAL_SIGN), data)) &&
                (inputData("mobileTel", MOBTEL_REGEXP, view.concatText(View.ENTER_MOB_TEL, View.EQUAL_SIGN), data))) {
            view.printMessage(View.DATA_VALID);
            model.setSubscriberData(data);
            view.printMessage(model.getSubscriberData().toString());
        }
    }

    /**
     * Input and validate data
     * @param key
     * @param regexp
     * @param message
     * @param dataContainer
     * @return
     */
    private boolean inputData(String key, String regexp, String message, HashMap dataContainer) {
        view.printMessageNoLine(message);
        String value = sc.nextLine();
        if (validateData(value, regexp)) {
            dataContainer.put(key, value);
            return true;
        }
        view.printMessage(View.DATA_INVALID);
        return false;
    }

    private boolean validateData(String value, String regex) {
        return value.matches(regex);
    }
}
