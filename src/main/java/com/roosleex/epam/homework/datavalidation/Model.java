package com.roosleex.epam.homework.datavalidation;

import java.util.HashMap;

public class Model {
    private HashMap<String, String> subscriberData = new HashMap<>();

    public HashMap<String, String> getSubscriberData() {
        return subscriberData;
    }

    public void setSubscriberData(HashMap<String, String> subscriberData) {
        this.subscriberData = subscriberData;
    }

    /**
     * Set one item of subscriber data.
     * @param key
     * @param value
     */
    public void setSubscriberDataItem(String key, String value) {
        subscriberData.put(key, value);
    }
}
