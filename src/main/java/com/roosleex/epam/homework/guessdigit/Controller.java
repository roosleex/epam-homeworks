package com.roosleex.epam.homework.guessdigit;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Controller {
    private Model model;
    private View view;
    public static final int RAND_MIN = 0;
    public static final int RAND_MAX = 100;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    public void processUser() {
        model.setBounds(RAND_MIN, RAND_MAX);
        model.generateValue();
        view.printMessage(View.FIRST_USER_TXT + View.BETWEEN_TXT + model.getMinBound() + View.AND_TXT + model.getMaxBound() + View.DOT_TXT);
        view.printMessage(View.SECOND_USER_TXT + View.DOT_TXT);

        int x;
        Scanner scan = new Scanner(System.in);
        while (true) {
            view.printMessage(View.INPUT_INT_TXT + View.BETWEEN_TXT + model.getMinBound() + View.AND_TXT + model.getMaxBound() + View.INCLUD_TXT + View.DOT_TXT);
            view.printMessageNoLine(View.X_TXT + View.EQUAL_TXT);
            if (!scan.hasNextInt()) {
                scan.nextLine();
                continue;
            }
            x = scan.nextInt();
            if (!checkOnRange(x)) {
                continue;
            }
            if (wasEntered(x)) {
                view.printMessage(x + View.WAS_ENTERED_TXT + View.DOT_TXT);
                continue;
            }
            if (model.checkValue(x)) {
                continue;
            }
            view.printMessage(View.CONGRATS_TXT + model.getValue());
            printStatistics();
            break;
        }

    }

    private boolean checkOnRange(int val) {
        if ((val < model.getMinBound()) || (val > model.getMaxBound())) {
            return false;
        }
        return true;
    }

    private boolean wasEntered(int val) {
        if (model.getEnteredNums().contains(val)) {
            return true;
        }
        return false;
    }

    private void printStatistics() {
        StringBuilder sb = new StringBuilder();
        List<Integer> enteredNums = model.getEnteredNums();
        for (Integer elem : enteredNums) {
            sb.append(elem + " ");
        }
        view.printMessage(View.ENTERED_NUMS_TXT + sb.toString());
        view.printMessage(View.TOTAL_ATTEMPTS_TXT + model.getAttemptsCount());
    }
}
