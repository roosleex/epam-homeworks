package com.roosleex.epam.homework.guessdigit;

import java.util.LinkedList;

public class Model {
    private int value;
    private int maxBound;
    private int minBound;
    private LinkedList<Integer> enteredNums = new LinkedList<>();
    private int attemptsCount = 0;

    public int getValue() {
        return value;
    }

    private void setValue(int value) {
        this.value = value;
    }

    public void generateValue() {
        setValue(rand());
    }

    private int rand(int min, int max) {
        int range = (max - min) + 1;
        return (int)(Math.random() * range) + min;
    }

    private int rand() {
        return rand(minBound, maxBound);
    }

    public int getMaxBound() {
        return maxBound;
    }

    public void setMaxBound(int maxBound) {
        this.maxBound = maxBound;
    }

    public int getMinBound() {
        return minBound;
    }

    public void setMinBound(int minBound) {
        this.minBound = minBound;
    }

    public boolean checkValue(int value) {
        enteredNums.add(value);
        attemptsCount++;
        if (value == this.value){
            return false;
        } else if (value > this.value){
            maxBound = value;
        } else {
            minBound = value;
        }
        return true;
    }

    public LinkedList<Integer> getEnteredNums() {
        return enteredNums;
    }

    public int getAttemptsCount() {
        return attemptsCount;
    }

    public void setBounds(int min, int max) {
        minBound = min;
        maxBound = max;
    }
}
