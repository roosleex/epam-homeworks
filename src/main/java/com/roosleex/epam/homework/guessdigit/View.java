package com.roosleex.epam.homework.guessdigit;

public class View {
    public static final String FIRST_USER_TXT = "The program has conceived a digit";
    public static final String SECOND_USER_TXT = "Try to guess it";
    public static final String BETWEEN_TXT = " between ";
    public static final String AND_TXT = " and ";
    public static final String INPUT_INT_TXT = "Input int value";
    public static final String DOT_TXT = ".";
    public static final String INCLUD_TXT = " (including)";
    public static final String WAS_ENTERED_TXT = " was already entered";
    public static final String MORE_TXT = "Conceived digit is more";
    public static final String LESS_TXT = "Conceived digit is less";
    public static final String CONGRATS_TXT = "Congratulations!!! You have just guessed the conceived digit. It was ";
    public static final String ENTERED_NUMS_TXT = "Entered numbers: ";
    public static final String TOTAL_ATTEMPTS_TXT = "Total attempts: ";
    public static final String EQUAL_TXT = " = ";
    public static final String X_TXT = "x";

    public void printMessage(String message) {
        System.out.println(message);
    }
    public void printMessageNoLine(String message) {
        System.out.print(message);
    }
}
